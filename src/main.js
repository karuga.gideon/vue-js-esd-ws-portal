/* eslint-disable prettier/prettier */
import Vue from "vue";
import App from "./App";
import router from "./router/index";

import PaperDashboard from "./plugins/paperDashboard";
import axios from "axios";
import "vue-notifyjs/themes/default.css";

Vue.use(PaperDashboard);
Vue.prototype.$axios = axios;

/* eslint-disable no-new */
new Vue({
    router,
    render: h => h(App)
}).$mount("#app");